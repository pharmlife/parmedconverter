from collections import Iterable
import parmed as pmd
import simtk.openmm as mm
import simtk.openmm.app as app
import simtk.unit as u
from abc import ABC, abstractmethod

DEFAULT_SYSTEM_ARGS = {
    "nonbondedMethod": app.CutoffNonPeriodic,
    "nonbondedCutoff": u.Quantity(value=8.0, unit=u.angstrom),
    "switchDistance": u.Quantity(value=0.0, unit=u.angstrom),
    "constraints": None,
    "rigidWater": False,
    "implicitSolvent": app.OBC2,
    "implicitSolventKappa": None,
    "implicitSolventSaltConc": u.Quantity(value=0.0, unit=u.mole / u.liter),
    "temperature": u.Quantity(value=298.15, unit=u.kelvin),
    "soluteDielectric": 1.0,
    "solventDielectric": 78.5,
    "useSASA": False,
    "removeCMMotion": True,
    "hydrogenMass": None,
    "ewaldErrorTolerance": 0.0005,
    "flexibleConstraints": False,
    "verbose": False,
    "splitDihedrals": False
}

DEFAULT_INTEGRATOR = mm.LangevinIntegrator(300*u.kelvin, 1/u.picosecond, 0.002*u.picosecond)
DEFAULT_PLATFORM = mm.Platform.getPlatformByName('CPU')

DEFAULT_RESTRAINT_K = 5.0
_RESTRAINT_UNITS = u.kilocalories_per_mole/u.angstroms**2

class ParmedConverterMeta(ABC):
    def __init__(self):
        pass

    @classmethod
    @abstractmethod
    def load_structure(cls, parameters: str, coordinates: str) -> pmd.Structure:
        pass

    @classmethod
    @abstractmethod
    def strip_atoms(cls, structure: pmd.Structure, strip_mask: str) -> pmd.Structure:
        pass

    @classmethod
    @abstractmethod
    def create_system(cls, structure: pmd.Structure) -> pmd.Structure:
        pass

    @classmethod
    @abstractmethod
    def add_harmonic_restraints(cls, structure: pmd.Structure, system: mm.System, restraint_ids: Iterable[int]) -> pmd.Structure:
        pass

    @classmethod
    @abstractmethod
    def create_simulation(cls, struct: pmd.Structure, system: mm.System) -> app.Simulation:
        pass

    @classmethod
    @abstractmethod
    def initialize_simulation(cls, struct: pmd.Structure, simulation: app.Simulation) -> None:
        pass

    @classmethod
    @abstractmethod
    def test_simulation(cls, simulation, steps=100) -> bool:
        pass


class ParmedConverterBase(ParmedConverterMeta):

    def __init__(self):
        super().__init__()
        self._system_arguments = DEFAULT_SYSTEM_ARGS
        self._integrator = DEFAULT_INTEGRATOR
        self._platform = DEFAULT_PLATFORM
        self._restraint_k = u.Quantity(DEFAULT_RESTRAINT_K, _RESTRAINT_UNITS)

    @classmethod
    def load_structure(cls, parameters: str, coordinates: str) -> object:
        """
        Basic function for loading a parmed Structure instance from file. Users are responsible for ensuring the inputs
        result in a Structure with (a) parameters and (b) coordinates assigned correctly. As a result, it is recommended
        that only amber or gromacs parameter files are handed to this function (.prmtop, .parm7, .top).

        Parameters:
            parameters (str): Filepath for a parmed compatible parameter file (.parm7, .prmtop, .top).
            coordinates (str): Filepath for a parmed compatible coordinates file.

        Returns:
            structure (object): The created parmed structure.

        """

        structure = pmd.load_file(parameters, xyz=coordinates, structure=True)
        return structure

    @classmethod
    def strip_atoms(cls, structure: pmd.Structure, strip_mask: str) -> None:
        """
        Strip atoms from a parmed Structure, e.g. solvent molecules. Care: original Structure instance will be modified.

        Parameters:
            structure (pmd.Structure): parmed Structure instance to be stripped.
            strip_mask (str): Amber mask selecting the atoms to be stripped.

        Returns:
            None

        """

        structure.strip(strip_mask)

    @classmethod
    def create_system(cls, structure: pmd.Structure) -> mm.System:
        """
        Create an OpenMM System using a parmed Structure. Structure must be properly parameterized. The settings for the
        System will be derived from the system_arguments property associated with the instance of this class.

        Parameters:
            structure (pmd.Structure): parmed Structure instance to be converted into a system. Must be parameterized.

        Returns:
            system (mm.System): The created OpenMM System object.

        """
        return structure.createSystem(kwargs=cls.system_arguments)

    @classmethod
    def add_harmonic_restraint(cls, system: mm.System, structure: pmd.Structure, restraint_mask: str) -> None:
        """
        Add harmonic restraints to atoms in an OpenMM System, e.g. backbone restraints. Care: original System instance
        will be modified.

        Parameters:
            system (mm.System): OpenMM System instance to be modified.
            structure (pmd.Structure): The structure that the OpenMM System was created using. Must have coordinates.
            restraint_mask (str): An amber mask which selects the atoms the restraint is to be applied to.

        Returns:
            None

        """
        force = mm.CustomExternalForce("k*((x-x0)^2+(y-y0)^2+(z-z0)^2)")
        force.addGlobalParameter("k", 5.0 * u.kilocalories_per_mole / u.angstroms ** 2)
        force.addPerParticleParameter("x0")
        force.addPerParticleParameter("y0")
        force.addPerParticleParameter("z0")

        restraint_ixs = [a.index for a in structure[restraint_mask].atoms]
        xyz = structure.coordinates

        for i in restraint_ixs:
            force.addParticle(i, xyz[i].value_in_unit(u.nanometers))

        system.addForce(force)


    @classmethod
    def create_simulation(cls, system: mm.System, structure: pmd.Structure) -> app.Simulation:
        """
        Create an OpenMM Simulation using an OpenMM System. The integrator and platform used to initialize the
        Simulation  settings will be derived from the integrator and platform properties associated with the instance of
        this class.

        Parameters:
            structure (pmd.Structure): parmed Structure instance to be converted into a system. Must be parameterized.

        Returns:
            system (mm.System): The created OpenMM System object.

        """
        return app.Simulation(structure.topology, system, cls.integrator, cls.platform)

    @classmethod
    def initialize_simulation(cls, structure: pmd.Structure, simulation: app.Simulation) -> app.Simulation:
        """
        Create an OpenMM Simulation using an OpenMM System. The integrator and platform used to initialize the
        Simulation  settings will be derived from the integrator and platform properties associated with the instance of
        this class.

        Parameters:
            structure (pmd.Structure): parmed Structure instance to be converted into a system. Must be parameterized.

        Returns:
            system (mm.System): The created OpenMM System object.

        """
        simulation.context.setPositions(structure.coordinates)
        simulation.context.setVelocitiesToTemperature(simulation.integrator.getTemperature())

        simulation.minimizeEnergy()

        return simulation

    @classmethod
    def test_simulation(cls, simulation: app.Simulation, steps: int = 100):
        """
        Run an OpenMM Simulation for a set number of steps, resetting once done. Utility function to check whether
        simulation is stable.

        Parameters:
            simulation (app.Simulation): OpenMM Simulation instance to be converted into a system. Must be parameterized.
            steps (int): Number of steps to run for (default: 100).
        Returns:
            None.

        """
        checkpoint = simulation.context.createCheckpoint()

        try:
            simulation.step(steps)
        except:
            return False
        finally:
            simulation.context.loadCheckpoint(checkpoint)

        return True

    @property
    def system_arguments(self):
        return self._system_arguments

    @system_arguments.setter
    def system_arguments(self, system_arguments):
        self._system_arguments = system_arguments

    @property
    def integrator(self):
        return self._integrator

    @integrator.setter
    def integrator(self, integrator):
        self._integrator = integrator

    @property
    def platform(self):
        return self._platform

    @platform.setter
    def platform(self, platform):
        self._platform = mm.Platform(platform)

    @property
    def restraint_k(self):
        return self._restraint_k

    @restraint_k.setter
    def restraint_k(self, k):
        if type(k) is u.Quantity:
            k = k.value_in_unit(_RESTRAINT_UNITS)

        self._restraint_k = u.Quantity(k, _RESTRAINT_UNITS)



class ParmedAmberConverter(ParmedConverterBase):
    @classmethod
    def load_structure(cls, amber_parameters: str, coordinates: str) -> pmd.amber.AmberFormat:
        return pmd.amber.AmberFormat(amber_parameters, coordinates, structure=True)


class ParmedGromacsConverter(ParmedConverterBase):
    @classmethod
    def load_structure(cls, gromacs_parameters: str, coordinates: str) -> pmd.gromacs.GromacsTopologyFile:
        return pmd.gromacs.GromacsTopologyFile(gromacs_parameters, coordinates, structure=True)


"""
class ParmedConverter(ParmedConverterBase):
    @classmethod
    def load_structure(cls, parameters: str, coordinates: str) -> object:
        ext = parameters.split(".")[-1]
        if ext in ["prmtop", "parm7"]:
            return cls._load_amber(parameters, coordinates)
        elif ext == "top":
            return cls._load_gromacs(parameters, coordinates)

    @classmethod
    def _load_amber(cls, parameters: str, coordinates: str) -> pmd.amber.AmberFormat:
        return ParmedAmberConverter().load_structure(parameters, coordinates)

    @classmethod
    def _load_gromacs(cls, parameters: str, coordinates: str) -> pmd.gromacs.GromacsTopologyFile:
        return ParmedGromacsConverter().load_structure(parameters, coordinates)
"""